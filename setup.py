#!/usr/bin/env python
# coding: utf-8
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import setuptools

setuptools.setup(
    name='testube',
    author='Sergi Jimenez',
    author_email='tripledes@gmail.com',
    description='Validate Kubernetes based infrastructure',
    long_description=open(os.path.join(
        os.path.dirname(__file__), 'README.rst')).read(),
    url='https://gitlab.com/tripledes/testube',
    classifiers=[
        'Development Status :: Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: Information Technology',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: POSIX',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Software Development :: Testing',
        'Topic :: System :: Systems Administration',
        'Framework :: Pytest',
    ],
    packages=setuptools.find_packages(),
    entry_points={
        'pytest11': {
            'pytest11.testube=testube.plugin',
        },
    },
    python_requires='>=3.8',
    setup_requires=['setuptools_scm'],
    use_scm_version=True,
    install_requires=[
        'pytest>=6.1.1,<7.0.0',
        'openshift>=0.11.2,<1.0.0'
    ],
)
