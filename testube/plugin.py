import pytest
import logging
import testube
import testube.cluster


@pytest.fixture(scope="module")
def _testshift_cluster(request):
    return request.param


@pytest.fixture(scope="module")
def cluster(_testube_cluster):
    return _testube_cluster


cluster.__doc__ = testube.cluster.Cluster.__doc__


def pytest_addoption(parser):
    group = parser.getgroup("testube")
    group.addoption(
        "--kubeconfig",
        action="store",
        dest="kubeconfig",
        help="Kubernetes connection configuration file."
    )


def pytest_generate_tests(metafunc):
    if "_testube_cluster" in metafunc.fixturenames:
        if hasattr(metafunc.module, "testube_cluster"):
            cluster = metafunc.module.testube_cluster
        else:
            cluster = None

        if metafunc.config.option.kubeconfig is not None:
            kubeconfig = metafunc.config.option.kubeconfig
        else:
            kubeconfig = None

        params = testube.get_cluster(cluster, kubeconfig)

        # TODO(tripledes): consider finding a more meaningful ID, if any
        metafunc.parametrize(
            "_testube_cluster", [params], ids=["testube"], scope="module")


@pytest.mark.trylast
def pytest_configure(config):
    if config.option.verbose > 1:
        root = logging.getLogger()
        if not root.handlers:
            root.addHandler(logging.NullHandler())
        logging.getLogger("testube").setLevel(logging.DEBUG)
