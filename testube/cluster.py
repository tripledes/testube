import testube.resources
from kubernetes import client, config
from openshift.dynamic import DynamicClient


class Cluster(object):
    """Cluster is the actual object that gets passed down to all
    test cases that define it as argument (fixture) and provides
    access to all resources and modules.

    Args:
        kubeconfig (str): Path to the kubeconfig file.

    Attributes:
        kubeconfig (str): Path to the kubeconfig file.

    Raises:
        AttributeError: When either a resource or a module is not found.

    Examples:
        Defining the test cases using the cluster fixture as follows::

            def my_new_test_case(cluster):
                kernel = cluster.Kernel("node00")

            def my_other_fancy_test_case(cluster):
                pods = cluster.Pod(label_selector="app=myapp")
    """
    _cluster_cache = None

    def __init__(self, kubeconfig):
        self.kubeconfig = kubeconfig
        super(Cluster, self).__init__()

    @classmethod
    def get_cluster(cls, cluster, kubeconfig):
        cache = cls._cluster_cache
        if not cache:
            cache = cluster = cls(kubeconfig)
        return cache

    def connect(self):
        try:
            k8s_client = config.new_client_from_config(
                config_file=self.kubeconfig)
            return DynamicClient(k8s_client)
        except TypeError:
            raise RuntimeError(
                "Kubernetes connection details are missing, please refer to \
                    testube docs.")

    def __getattr__(self, name):
        if name in testube.resources.resources:
            resource_class = testube.resources.get_resource_class(name)
            obj = resource_class.get_resource(self)
            setattr(self, name, obj)
            return obj
        raise AttributeError("'{}' object has no attribute '{}'".format(
            self.__class__.__name__, name))


get_cluster = Cluster.get_cluster
