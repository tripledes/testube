import importlib

resources = {
    'Deployment': 'deployment:Deployment',
    'DS': 'ds:DS',
    'Pod': 'pod:Pod',
    'PV': 'pv:PV',
    'PVC': 'pvc:PVC',
    'Service': 'service:Service',
}


def get_resource_class(name):
    resname, classname = resources[name].split(':')
    resname = '.'.join([__name__, resname])
    resource = importlib.import_module(resname)
    return getattr(resource, classname)
