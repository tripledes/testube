class Resource(object):
    _cluster = None

    @classmethod
    def get_resource(cls, _cluster):
        klass = cls.get_resource_class(_cluster)
        return type(klass.__name__, (klass,), {
            "_cluster": _cluster,
            "_connect": _cluster.connect,
        })

    @classmethod
    def get_resource_class(cls, cluster):
        return cls


class InstanceResource(Resource):

    @classmethod
    def get_resource(cls, _cluster):
        klass = super(InstanceResource, cls).get_resource(_cluster)
        return klass()
