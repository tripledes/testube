from testube.utils import cached_property, ResourceNotFoundException
from testube.resources.base import Resource


class Pod(Resource):
    """Pod class instantiates an OCP pod resource.

    Args:
        name (str): Pod's name as found in pod.metadata
        namespace (str): Namespace where the pod is running
        label_selector (str): Label selector as used in oc command

    Raises:
        RuntimeError: When only namespace is passed as argument.
        ResourceNotFoundException: When OCP API returns resource not found.

    Examples:

        >>> pod = Pod(label_selector='pod-template-hash=79fc6c4cb7')
        >>> pod.exists
        True
        >>> pod.is_running
        True
    """

    def __init__(self, name=None, namespace=None, label_selector=None):
        if not name and not label_selector:
            raise RuntimeError(
              "Pod() requires either a name or a label_selector")

        if namespace and (not name or not label_selector):
            raise RuntimeError(
                "Pod(namespace) requires name"
            )

        if name and label_selector:
            raise RuntimeError("Pod() can only have name or label_selector")

        self.__name = name
        self.__namespace = namespace
        self.__label_selector = label_selector
        connection = self._connect()
        self.__resources = connection.resources.get(
            api_version='v1', kind='Pod')
        super(Pod, self).__init__()

    @cached_property
    def _pods(self):
        try:
            if not self.__namespace and not self.__label_selector:
                pods = self.__resources.get()
                for pod in pods.items:
                    if pod.metadata.name == self.__name:
                        return list([pod])
                return False
            rsrcs = self.__resources.get(
                name=self.__name, namespace=self.__namespace,
                label_selector=self.__label_selector
            )
            return [pod for pod in rsrcs.items]
        except ResourceNotFoundException:
            return False

    @property
    def exists(self):
        """bool: Whether pods were found or not."""
        if self._pods:
            return True
        return False

    @property
    def is_running(self):
        """bool: Check for the status phase of all the pods"""
        for pod in self._pods:
            if pod.status.phase != "Running":
                return False
        return True

    @property
    def count(self):
        """int: Amount of pods found by the search criteria"""
        return len(self._pods)
