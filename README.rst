#######################################
Testube tests your Kubernetes clusters
#######################################

Description
===========

Testube_ aims to ease the validation of Kubernetes_ based infrastructure,
using Pytest_ to verify the state of the resources defined in your cluster.

Testube takes it after Testinfra_, but instead of being a general purpose
infrastructure testing tool, Testube specializes exclusively in Kubernetes_.

Find more information in: Testube_.

.. _Pytest: https://pytest.org/
.. _Testinfra: https://testinfra.readthedocs.io/en/latest/
.. _Testube: https://testube.rtfd.io
.. _Kubernetes: https://kubernetes.io