Welcome to testube's documentation!
=====================================

testube aims to provide the means to validate Kubernetes based infrastructures, for this
purpose, testube is implemented as a pytest_ plugin, taking after Testinfra_ for inspiration.

Cluster authentication
----------------------

testube can use any of the following to find the cluster configuration file:

* Standard configuration location (*~/.kube/config*)
* *KUBECONFIG* environment variable
* *--kubeconfig* argument.

Usage examples
--------------

* Install it:

  .. code-block:: console

     $ pip install -U -e "git+https://gitlab.com/tripledes/testube.git#egg=testube"

* Run it:

  .. code-block:: python

     def test_sdn_pods_exist(cluster):
         pod = cluster.Pod(label_selector="app=sdn")
         assert pod.exists

     def test_etcd_pods_count(cluster):
         pod = cluster.Pod(label_selector="app=etcd")
         assert pod.count == 3

  .. code-block:: console

     $ pytest --kubeconfig=/home/sjr/.kube/config tests -v
     =========================================== test session starts ===========================================
     platform linux -- Python 3.8.5, pytest-6.1.1, py-1.9.0, pluggy-0.13.1 -- /home/sjr/.pyenv/versions/3.8.5/envs/testube-install/bin/python3.8
     cachedir: .pytest_cache
     rootdir: /home/sjr
     plugins: testube-0.1.dev4+g186b22e
     collected 2 items

     tests/node_test.py::test_sdn_pods_exist[testube] PASSED                                            [ 50%]
     tests/node_test.py::test_etcd_pods_count[testube] PASSED                                           [100%]

     ============================================ 2 passed in 0.28s ============================================


More examples
-------------

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Resource Examples

   examples/resources/*

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Module Examples

   examples/modules/*

.. toctree::
   :maxdepth: 1
   :caption: API

   autoapi/testube/index


.. _pytest: https://pytest.org/
.. _Testinfra: https://testinfra.readthedocs.io/en/latest/
